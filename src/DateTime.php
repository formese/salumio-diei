<?php

namespace SalumIo\Diei;

use DateTime as BaseDateTime;
use JsonSerializable;
use MongoDB\BSON\UTCDateTime;

class DateTime extends BaseDateTime implements JsonSerializable
{
    /**
     * First day of week.
     *
     * @var int
     */
    protected static $weekStartsAt = 1;

    /**
     * Last day of week.
     *
     * @var int
     */
    protected static $weekEndsAt = 0;

    /**
     * Days of weekend.
     *
     * @var array
     */
    protected static $weekendDays = array(
        6,
        0,
    );

    /**
     * Create a datetime instance from a string.
     *
     * This is an alias for the constructor that allows better fluent syntax
     * as it allows you to do DateTime::parse('Monday next week')->fn() rather
     * than (new DateTime('Monday next week'))->fn().
     *
     * @param string|null               $time
     * @param \DateTimeZone|string|null $tz
     *
     * @return static
     */
    public static function parse($time = null, $tz = null)
    {
        return new static($time, $tz);
    }

    /**
     * Get a datetime instance for the current date and time.
     *
     * @param \DateTimeZone|string|null $tz
     *
     * @return static
     */
    public static function now($tz = null)
    {
        return new static(null, $tz);
    }

    public static function cast($obj)
    {
        if ($obj instanceof BaseDateTime) {
            return new self($obj->format('c'), $obj->getTimeZone());
        } elseif ($obj instanceof UTCDateTime) {
            return new self('@' . (((string) $obj) / 1000));
        } elseif (is_int($obj)) {
            return new self('@' . $obj);
        }
        return new self($obj);
    }

    /**
     * Determines if the instance is a weekday
     *
     * @return bool
     */
    public function isWeekday()
    {
        return !$this->isWeekend();
    }
    /**
     * Determines if the instance is a weekend day
     *
     * @return bool
     */
    public function isWeekend()
    {
        return in_array($this->format('w'), static::$weekendDays);
    }

    public function jsonSerialize()
    {
        return $this->format('c');
    }
}
